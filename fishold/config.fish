if status is-interactive
    # Commands to run in interactive sessions can go here
	neofetch
    # bluetoothctl power on
end

# set MANPAGER
# set -gx MANPAGER hx
# Stuff for flatpak
set -l xdg_data_home $XDG_DATA_HOME ~/.local/share
set -gx --path XDG_DATA_DIRS $xdg_data_home[1]/flatpak/exports/share:/var/lib/flatpak/exports/share:/usr/local/share:/usr/share:~/.nix-profile/share/applications

for flatpakdir in ~/.local/share/flatpak/exports/bin /var/lib/flatpak/exports/bin ~/.nix-profile/bin
    if test -d $flatpakdir
        contains $flatpakdir $PATH; or set -a PATH $flatpakdir
    end
end

#Gnome Keyring
if test -n "$DESKTOP_SESSION"
    for env_var in (gnome-keyring-daemon --start)
        set -x (echo $env_var | string split "=")
    end
end

# Functions needed for !! and !$
function __history_previous_command
  switch (commandline -t)
  case "!"
    commandline -t $history[1]; commandline -f repaint
  case "*"
    commandline -i !
  end
end

function __history_previous_command_arguments
  switch (commandline -t)
  case "!"
    commandline -t ""
    commandline -f history-token-search-backward
  case "*"
    commandline -i '$'
  end
end
# The bindings for !! and !$
if [ $fish_key_bindings = fish_vi_key_bindings ];
  bind -Minsert ! __history_previous_command
  bind -Minsert '$' __history_previous_command_arguments
else
  bind ! __history_previous_command
  bind '$' __history_previous_command_arguments
end

set EDITOR "hx"
#pacman stuff 
# abbr pacman 'sudo pacman --color=auto'
# abbr search 'pacman -Ss --color=auto'
# abbr install 'sudo pacman -S'
# abbr remove 'sudo pacman -Rns'
# abbr update 'sudo pacman -Syyuu'
# abbr aur 'pikaur -Sua'
#abbr aur 'aura -A'


#Genoo stuff
# abbr update 'doas emerge --update --deep --changed-use @world'
# abbr search 'emerge -s'
# abbr install 'doas emerge'
# abbr query 'equery f'
# abbr use 'equery u'
# abbr sync 'doas emaint sync -a'
# abbr depclean 'doas emerge --depclean'
# abbr portage 'doas emerge --oneshot sys-apps/portage'
# abbr go 'doas emerge @golang-rebuild'

#void package manager stuff
abbr search 'xbps-query -Rs'
abbr install 'doas xbps-install -S'
abbr query 'xbps-query -f'
abbr remove 'doas xbps-remove -R'
abbr orphan 'doas xbps-remove -o'
abbr update 'doas xbps-install -Suv'

# dnf stuff
# abbr install 'doas dnf install'
# abbr search 'dnf search'
# abbr query 'rpm -ql'
# abbr update 'doas dnf update'

abbr cp 'cp -rv'
abbr rm 'trash'
abbr mount 'udisksctl mount -b'
# abbr update-grub 'doas grub-mkconfig -o /boot/grub/grub.cfg'
abbr ogg 'yt-dlp --extract-audio --add-metadata --audio-format vorbis --audio-quality 256k '
abbr unity 'cd /home/zoinkshaggy/Downloads/UnityModManager && mono /home/zoinkshaggy/Downloads/UnityModManager/UnityModManager.exe' 
abbr pathfinder 'steam-native steam://rungameid/640820'
abbr trails 'MANGOHUD=1 steam-native steam://rungameid/251150'
abbr berseria 'MANGOHUD=1 steam-native steam://rungameid/429660'
abbr horace 'MANGOHUD=1 steam-native steam://rungameid/629090'
# abbr steam 'MANGOHUD=1 steam-native' 
abbr chasm 'MANAGOHUD=1 steam-native steam://rungameid/312200' 
abbr rise 'MANGOHUD=1 steam-native steam://rungameid/391220'
abbr tomb 'MANGOHUD=1 steam-native steam://rungameid/203160'
abbr starocean 'MANGOHUD=1 steam-native steam://rungameid/609150'
abbr cl clear
abbr .c ~/.config/
abbr emacs 'emacs -nw'
abbr weather 'curl wttr.in'
abbr ls 'exa -lhF --icons --color=always --group-directories-first'
abbr brave 'flatpak run com.brave.Browser'

abbr xdeb 'xdeb -Sde'
