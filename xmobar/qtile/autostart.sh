#!/usr/bin/env bash 

xrandr --output DisplayPort-1 --mode 3440x1440 --rate 120 &
lxsession &
picom &
nitrogen --restore &
/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
volumeicon &
nm-applet &
caffeine &
