# Imports   
import os
import re
import socket
import subprocess
from libqtile import qtile
from libqtile.config import Click, Drag, Group, KeyChord, Key, Match, Screen
from libqtile.command import lazy
from libqtile import layout, bar, widget, hook
from libqtile.lazy import lazy
from typing import List  # noqa: F401

# Autostart script from ~/.config/qtile/autostart.sh
@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/autostart.sh'])

mod = "mod4"
mod1 = "mod1"
mod2 = "control"
mod3 = "shift"
#home =  os.path.expanduser('~')
terminal = "alacritty"

keys =[
    # Change window focus
    Key([mod], "Left", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "Right", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "Down", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "Up", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "Tab", lazy.layout.next(),desc="Move window focus to other window"),

    # Move windows
    Key([mod, "shift"], "Left", lazy.layout.shuffle_left(), desc="Move window to the left"),
    Key([mod, "shift"], "Right", lazy.layout.shuffle_right(), desc="Move window to the right"),
    Key([mod, "shift"], "Down", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "Up", lazy.layout.shuffle_up(), desc="Move window up"),

    #Resize layout
    Key([mod, "control"], "Left", lazy.layout.shrink(), lazy.layout.decrease_nmaster(), desc="Grow window to the left"),
    Key([mod, "control"], "Right", lazy.layout.grow(), lazy.layout.increase_nmaster(), desc="Grow window to the right"),
    Key([mod, "control"], "Down", lazy.layout.grow_down(), desc="Grow window down"),
    Key([mod, "control"], "Up", lazy.layout.grow_up(), desc="Grow window up"),

    # Toggle window states
    Key([mod, "shift"], "t", lazy.window.toggle_floating()),
    Key([mod], "f", lazy.window.toggle_fullscreen(), desc='toggle fullscreen'),
    Key([mod, "shift"], "m", lazy.layout.maximize()),
    Key([mod], "m", lazy.window.toggle_maximize(), desc='toggle window between minimum and maximum sizes'),

    # Launch user defined terminal
    Key([mod], "Return", lazy.spawn(terminal+" -e fish"), desc="Launch terminal"),

    # Toggle between different layouts as defined below
    Key([mod], "space", lazy.next_layout(), desc="Toggle between layouts"),

    # Keybinding to kill focused windows
    Key([mod], "x", lazy.window.kill(), desc="Kill focused window"),
    Key([mod, "shift"], "c", lazy.window.kill(), desc="Kill focused window"),

    # Restart or Quit Qtile
    Key([mod], "q", lazy.restart(), desc="Restart Qtile"),
    Key([mod, "shift"], "q", lazy.shutdown(), desc="Shutdown Qtile"),

    # Keybindings to launch user defined programs
    Key([mod], "p", lazy.spawn("dmenu_run -fn 'Ubuntu-16' -nb '#282a2e' -sb '#5f819d'"), desc="launch dmenu"),
    Key([mod], "w", lazy.spawn("firefox-esr"), desc="launch firefox-esr"),
    #Key([mod], "f", lazy.spawn("pcmanfm"), desc="launch pcmanfm"),

    # Audio controls
    Key([], "XF86AudioMute", lazy.spawn("amixer -D pulse sset Master toggle")),
    Key([], "XF86AudioLowerVolume", lazy.spawn("amixer -D pulse sset Master 5%-")),
    Key([], "XF86AudioRaiseVolume", lazy.spawn("amixer -D pulse sset Master 5%+")),
    Key([mod], "XF86AudioRaiseVolume", lazy.spawn("pactl set-sink-volume @DEFAULT_SINK@ +5%")),

    # Brightness Controls
    Key([], "XF86MonBrightnessUp", lazy.spawn("xrandr --output DisplayPort-1 --brightness 0.7")),
    Key([], "XF86MonBrightnessDown", lazy.spawn("xrandr --output DisplayPort-1 --brightness 0.5")),
    Key([mod], "XF86MonBrightnessUp", lazy.spawn("xrandr --output DisplayPort-1 --brightness 1.0")),
    Key([mod], "r", lazy.spawncmd(), desc="Spawn a command using a prompt widget"),

    # ALT KEY Keybindings to launch alternative programs
    Key([mod1], "b", lazy.spawn("wal"), desc="launch wal script"),
    Key(["control", "shift"], "e", lazy.spawn("emacsclient -c -a emacs"), desc='Doom Emacs'),
    Key([mod1], "f", lazy.spawn("st -e vifm"), desc="launch vifm"),
    Key([mod1], "w", lazy.spawn("brave-browser"), desc="launch brave-browser"),
    Key([mod1], "e", lazy.spawn("geany"), desc="launch geany"),
    Key([mod], "y", lazy.spawn("scrot"), desc="take screenshot"),
    Key([mod], "s", lazy.spawn("xmenu.sh && scrot"), desc="launch xmenu"),

    #Screensaver
    Key([mod], "g", lazy.spawn("xset s off -dpms"), desc="Turn off Screensaver"),
    Key([mod, "shift"], "g", lazy.spawn("xset s on +dpms"), desc="Turn on Screensaver"),

]


groups= [
                Group("1",
                      label="",
                      ),

                Group("2",
                      label="",
                      ),

                Group("3",
                      label="",
                      ),

                Group("4",
                      label="",
                      ),

                Group("5",
                      label=""),

                Group("6",
                      label="",
                      ),

                Group("7",
                      label="",
                      ),

                Group("8",
                      label=""),

                Group("9",
                      label="",
                      matches=[Match(wm_class=["zoom"]),
                               ],
                      ),

                Group("0",
                      label=""),
]

for i in range(len(groups)):
   keys.append(Key([mod], str((i)), lazy.group[str(i)].toscreen()))
   keys.append(Key([mod, "shift"], str((i)), lazy.window.togroup(str(i), switch_group=True)))

#LAYOUTS
layouts = [
    # layout.Stack(num_stacks=2),
    # layout.Columns(margin=7, border_width=4, border_focus="#ffffff", border_normal="#4c566a", ),
    # layout.Matrix(),
    # layout.RatioTile(margin=7)
    # layout.Tile(margin=7, border_width=3, border_focus="#ffffff", border_normal="#4c566a", new_client_position='top', ratio=0.55),
    # layout.VerticalTile(),
    # layout.Zoomy(
    #    margin=7,
    #    columnwidth=300,
    #),
    #layout.TreeTab(
    #        active_bg = 'ffffff',
    #        active_fg = '000000',
    #        bg_color = '293136',
    #        font = 'novamono for powerline',
    #        fontsize = 15,
    #        panel_width = 200,
    #        inactive_bg = 'a1acff',
    #        inactive_fg = '000000',
    #        sections = ['Qtile'],
    #        section_fontsize = 18,
    #       section_fg = 'ffffff',
    #        section_left = 70
    #),
    layout.MonadTall(margin=5, border_width=3, border_focus="red", border_normal="#4c566a"),
    layout.MonadWide(margin=5, border_width=3, border_focus="#bc7cf7", border_normal="#4c566a"),
    layout.Bsp      (margin=5, border_width=3, border_focus="#bc7cf7", border_normal="#4c566a", fair=False),
    layout.Max(),
]


colors =  [

        ["#1c1c1c", "#1c1c1c"], # color 0
        ["#373b41", "#373b41"], # color 1
        ["#c5c8c6", "#c5c8c6"], # color 2
        ["#a54242", "#a54242"], # color 3
        ["#5f819d", "#5f819d"], # color 4
        ["#373b41", "#373b41"], # color 5
        ["#b294bb", "#b294bb"], # color 6
        ["#81a2be", "#81a2be"], # color 7
        ["#e2c5dc", "#e2c5dc"], # color 8
        ["#5e8d87", "#5e8d87"]] # color 9

widget_defaults = dict(
    font='Source Code Pro',
    fontsize=18,
    padding=3,
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=bar.Bar(
            [
                widget.TextBox(
                    text=" ✏️",
                    background=colors[3],
                    foreground=colors[0],
                    mouse_callbacks={'Button1': lambda: qtile.cmd_spawn('emacsclient -c -a emacs /home/zoinkshaggy/.config/qtile/config.py')},
                    ),
                widget.TextBox(
                   text='\ue0be',
                   fontsize='33',
                   padding=0,
                   background=colors[3],
                   foreground=colors[0],
                ),

                #widget.TextBox(
                #    text='|',
                #    padding=0,
                #    foreground=colors[5],
                #),

                widget.GroupBox(
                    font="Source Code Pro",
                    fontsize=20,
                    margin_y=3,
                    margin_x=6,
                    padding_y=7,
                    padding_x=6,
                    borderwidth=4,
                    active=colors[4],
                    inactive=colors[2],
                    rounded=False,
                    highlight_color=colors[2],
                    highlight_method="block",
                    this_current_screen_border=colors[7],
                    block_highlight_text_color=colors[0],
                ),

                #widget.TextBox(
                #    text='|',
                #    padding=0,
                #    foreground=colors[5],
                #),

                widget.Prompt(
                    background=colors[8],
                    foreground=colors[0],
                ),

                widget.Chord(
                    chords_colors={
                        'launch': ("#ff0000", "#ffffff"),
                    },
                        name_transform=lambda name: name.upper(),
                ),

                widget.TextBox(
                    text='\ue0be',
                    fontsize='33',
                    padding=0,
                    background=colors[0],
                    foreground=colors[4],
                ),

                widget.WindowName(
                    background=colors[4],
                        ),

                widget.TextBox(
                    text='\ue0be',
                    fontsize='33',
                    padding=0,
                    background=colors[4],
                    foreground=colors[0],
                ),


                widget.Spacer(),

               widget.TextBox(
                    text='\ue0be',
                    fontsize='33',
                    padding=0,
                    background=colors[0],
                    foreground=colors[7],
                ),

                widget.CurrentLayoutIcon(
                    custom_icon_paths=[os.path.expanduser("~/.config/qtile/icons")],
                    scale=0.6,
                    padding=0,
                    background=colors[7],
                ),

                widget.CurrentLayout(
		background=colors[7],
                ),

                 widget.TextBox(
                    text='\ue0be',
                    fontsize='33',
                    padding=0,
                    background=colors[7],
                    foreground=colors[0],
                ),

                widget.TextBox(
                    text = "🛠️ ",
                    foreground = colors[3],
                    background = colors[0],
                    mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(terminal + ' -e sudo pacman -Syyuu')},
                ),

                widget.CheckUpdates(
                    update_interval = 300,
                    distro = "Arch_checkupdates",
                    display_format = "{updates} Updates ",
                    foreground = colors[3],
                    mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(terminal + ' -e sudo pacman -Syyuu')},
                    background = colors[0],
                ),
                
                widget.TextBox(
                    text='\ue0be',
                    fontsize='33',
                    padding=0,
                    background=colors[0],
                    foreground=colors[3],
                ),

                widget.TextBox(
	            text = ' ',
                    fontsize = 33,
                    padding=0,
                    background=colors[3],
                ),

                widget.DF(
	            fmt = '{}',
	            partition = '/home',
	            format = '{uf}{m} ({r:.0f}%)',
	            visible_on_warn = False,
	            background = colors[3],
	            padding = -1,
	            mouse_callbacks={'Button1': lambda: qtile.cmd_spawn('xdiskusage')},
				),

                widget.TextBox(
                    text='\ue0be',
                    fontsize='33',
                    padding=0,
                    background=colors[3],
                    foreground=colors[0],
                ),

                widget.Systray(
                    background=colors[0],
                    icons_size=33,
                    padding=4,
                ),

                widget.Sep(
                    padding=8,
                    linewidth=0,
                    background=colors[0],
                ),

                widget.TextBox(
                    text='\ue0be',
                    fontsize='33',
                    padding=0,
                    background=colors[0],
                    foreground=colors[5],
                ),

                widget.Sep(
                    padding=6,
                    linewidth=0,
                    background=colors[5],
                ),

                widget.TextBox(
                    text=" ",
                    fontsize=20,
                    background=colors[5],
                    padding=-1,
                    mouse_callbacks={'Button1': lambda: qtile.cmd_spawn(terminal + " -e htop")},
                ),

                widget.Memory(
                    background=colors[5],
                    format='{MemUsed: .0f} MB',
                    mouse_callbacks={'Button1': lambda: qtile.cmd_spawn(terminal + " -e htop")},
                ),

                widget.TextBox(
                    text='\ue0be',
                    fontsize='33',
                    padding=0,
                    background=colors[5],
                    foreground=colors[9],
                ),

                widget.TextBox(
                    text="🔊",
                    background=colors[9],
                    foreground=colors[0],
                    padding=3,
                    mouse_callbacks={'Button3': lambda: qtile.cmd_spawn('pavucontrol')},
                ),

                widget.Volume(
                    background=colors[9],
                    mouse_callbacks={'Button3': lambda: qtile.cmd_spawn('pavucontrol')},
                    padding=3,
                ),

                widget.TextBox(
                    text='\ue0be',
                    padding=0,
                    background=colors[9],
                    foreground=colors[4],
                ),

                widget.Sep(
                    padding=10,
                    linewidth=0,
                    background=colors[4],
                ),

                widget.TextBox(
                    text='',
                    fontsize='20',
                    background=colors[4],
                    mouse_callbacks={'Button1': lambda: qtile.cmd_spawn(terminal + " -e calcurse")},
                ),

                widget.Clock(
                    background=colors[4],
                    format=' %d %b,%A',
                    mouse_callbacks={'Button1': lambda: qtile.cmd_spawn(terminal + " -e calcurse")},
                    padding=0,
                ),

                widget.Sep(
                    padding=6,
                    linewidth=0,
                    background=colors[4],
                ),

                widget.TextBox(
                    text='\ue0be',
                    fontsize='33',
                    padding=0,
                    background=colors[4],
                    foreground=colors[7],
                ),

                widget.TextBox(
                    text=' ',
                    font="Source Code Pro:style=Bold",
                    fontsize='20',
                    padding=0,
                    background=colors[7],
                ),

                widget.Clock(
                    font="Source Code Pro:style=Bold",
                    background=colors[7],
                    format='%I:%M %p',
                    padding=3,
                ),

                                
                widget.TextBox(
                    text='⏻',
                    fontsize='30',
                    font="Source Code Pro:style=Bold",
                    padding=8,
                    background=colors[7],
                    mouse_callbacks={'Button1': lambda: qtile.cmd_spawn(terminal + " -e reboot")},
                ),

                widget.Sep(
                    padding=6,
                    linewidth=0,
                    background=colors[7],
                ),
            ],
        26,
            opacity=0.95,
            background=colors[0],
            #margin=[8,2,0,2]
            ),
       ),
    ]

def window_to_prev_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i - 1].name)

def window_to_next_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i + 1].name)

def window_to_previous_screen(qtile):
    i = qtile.screens.index(qtile.current_screen)
    if i != 0:
        group = qtile.screens[i - 1].group.name
        qtile.current_window.togroup(group)

def window_to_next_screen(qtile):
    i = qtile.screens.index(qtile.current_screen)
    if i + 1 != len(qtile.screens):
        group = qtile.screens[i + 1].group.name
        qtile.current_window.togroup(group)

def switch_screens(qtile):
    i = qtile.screens.index(qtile.current_screen)
    group = qtile.screens[i - 1].group
    qtile.current_screen.set_group(group)

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False

#Floating rules
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    *layout.Floating.default_float_rules,
    Match(wm_class='confirmreset'),  # gitk
    Match(wm_class='makebranch'),  # gitk
    Match(wm_class='maketag'),  # gitk
    Match(wm_class='ssh-askpass'),  # ssh-askpass
    Match(title='branchdialog'),  # gitk
    Match(title='pinentry'),  # GPG key password entry
])
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

wmname = "LG3D"
