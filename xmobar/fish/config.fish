if status is-interactive
    # Commands to run in interactive sessions can go here
	neofetch
end
# Functions needed for !! and !$
function __history_previous_command
  switch (commandline -t)
  case "!"
    commandline -t $history[1]; commandline -f repaint
  case "*"
    commandline -i !
  end
end

function __history_previous_command_arguments
  switch (commandline -t)
  case "!"
    commandline -t ""
    commandline -f history-token-search-backward
  case "*"
    commandline -i '$'
  end
end
# The bindings for !! and !$
if [ $fish_key_bindings = fish_vi_key_bindings ];
  bind -Minsert ! __history_previous_command
  bind -Minsert '$' __history_previous_command_arguments
else
  bind ! __history_previous_command
  bind '$' __history_previous_command_arguments
end

#pacman stuff 
abbr pacman 'sudo pacman --color=auto'
abbr search 'pacman -Ss --color=auto'
abbr install 'sudo pacman -S '
abbr remove 'sudo pacman -Rns '
abbr update 'sudo pacman -Syyuu'
abbr aur 'pikaur -Sua'
abbr update-grub 'sudo grub-mkconfig -o /boot/grub/grub.cfg'
abbr ogg 'youtube-dl --extract-audio --add-metadata --audio-format vorbis --audio-quality 256k '
abbr unity 'cd /home/zoinkshaggy/Downloads/UnityModManager && mono /home/zoinkshaggy/Downloads/UnityModManager/UnityModManager.exe' 
abbr pathfinder 'steam-native steam://rungameid/640820'
abbr trails 'MANGOHUD=1 steam-native steam://rungameid/251150'
abbr berseria 'MANGOHUD=1 steam-native steam://rungameid/429660'
abbr horace 'MANGOHUD=1 steam-native steam://rungameid/629090'
abbr steam 'MANGOHUD=1 steam-native' 
abbr chasm 'MANAGOHUD=1 steam-native steam://rungameid/312200' 
abbr rise 'MANGOHUD=1 steam-native steam://rungameid/391220'
abbr tomb 'MANGOHUD=1 steam-native steam://rungameid/203160'
abbr starocean 'MANGOHUD=1 steam-native steam://rungameid/609150'
abbr cl clear
abbr .c .config/
abbr vim nvim
