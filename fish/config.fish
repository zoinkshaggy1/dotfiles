if status is-interactive
    # Commands to run in interactive sessions can go here
end
fastfetch

# Functions needed for !! and !$
function __history_previous_command
    switch (commandline -t)
        case "!"
            commandline -t $history[1]
            commandline -f repaint
        case "*"
            commandline -i !
    end
end

function __history_previous_command_arguments
    switch (commandline -t)
        case "!"
            commandline -t ""
            commandline -f history-token-search-backward
        case "*"
            commandline -i '$'
    end
end
# The bindings for !! and !$
if [ $fish_key_bindings = fish_vi_key_bindings ]
    bind -Minsert ! __history_previous_command
    bind -Minsert '$' __history_previous_command_arguments
else
    bind ! __history_previous_command
    bind '$' __history_previous_command_arguments
end

set -gx EDITOR hx
# For command not found
source /usr/share/doc/find-the-command/ftc.fish

# Update Grub
# abbr update-grub "sudo grub2-mkconfig -o /boot/grub2/grub.cfg"
abbr update-grub "sudo grub-mkconfig -o /boot/grub/grub.cfg"
# abbr update-grub 'sudo update-grub'

# Pacman stuff
# abbr update 'doas pacman -Syyuu'
# abbr search 'yay '
# abbr query 'pacman -Ql '
# abbr install 'doas pacman -S '
# abbr remove 'doas pacman -Rns '

# DNF stuff
# abbr update 'sudo dnf update --refresh'
# abbr install 'sudo dnf install'
# abbr search 'dnf search'
# abbr query 'dnf repoquery -l'
# abbr remove 'sudo dnf remove'

# XBS stuff
# abbr update 'sudo xbps-install -Su'
# abbr search 'xbps-query -Rs'
# abbr query 'xbps-query -f'
# abbr install 'sudo xbps-install'
# abbr remove 'sudo xbps-remove -R'

# zypper stuff
# abbr update 'sudo zypper refresh && sudo zypper dup'
# abbr vendor 'sudo zypper dup --allow-vendor-change'
# abbr search 'zypper search'
# abbr install 'sudo zypper install'
# abbr remove 'sudo zypper remove -u'
# abbr query 'rpm -ql'

# Gentoo stuff
abbr sync 'sudo emaint -a sync'
abbr update 'sudo emaint -a sync && sudo emerge --update --deep --newuse @world'
abbr query 'equery f'
abbr emerge 'sudo emerge'
abbr depclean 'sudo emerge --depclean'

# Terminal Abbreviations
abbr ogg 'yt-dlp --extract-audio --add-metadata --audio-format vorbis --audio-quality 256k '
abbr cl clear
abbr ls 'eza --group-directories-first -lh --icons'
abbr cat bat
# abbr hx helix
abbr hypr 'dbus-run-session Hyprland'
