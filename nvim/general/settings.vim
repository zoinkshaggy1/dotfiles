set number
set relativenumber
syntax enable
set mouse=a
set clipboard=unnamedplus
set tabstop=4 softtabstop=0 expandtab shiftwidth=4 smarttab 
