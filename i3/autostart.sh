#!/usr/bin/env bash

# run sxhkd -c ~/.config/sxhkd/sxhkdrc ~/.config/sxhkd/apps.sh
feh --bg-scale ~/Pictures -z &
xsetroot -cursor_name left_ptr &
killall picom
picom &
xrandr --output DisplayPort-1 --mode 3440x1440 --rate 100 --brightness 0.7 --set TearFree on &
killall lxpolkit
lxpolkit &
killall pipewire
# pipewire &
gentoo-pipewire-launcher &
killall pasystray
# run pasystray
# pasystray
killall polybar &
~/.config/polybar/launch.sh &
# bluetoothctl power on &
killall mpd
mpd &
killall tilda
tilda &
