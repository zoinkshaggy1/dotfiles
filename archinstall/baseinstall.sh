#! /bin/bash
cp pacman.conf /etc/
pacman -Syy
timedatectl set-ntp true
pacstrap /mnt base base-devel efibootmgr linux-zen linux-zen-headers linux-firmware grub os-prober networkmanager 
genfstab -U /mnt >> /mnt/etc/fstab
arch-chroot /mnt
ln -sf /usr/share/zoneinfo/America/Denver /etc/localtime
hwclock --systohc
echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" >> /etc/locale.conf
echo "arch" >> /etc/hostname
passwd

systemctl enable NetworkManager
useradd -m -G wheel,video zoinkshaggy -s /bin/fish
