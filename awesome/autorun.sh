#!/usr/bin/env bash

function run {
    if ! pgrep -f $1 ;
    then
        $@&
    fi
}
#run dunst
run picom
run xrandr --output DisplayPort-1 --mode 3440x1440 --rate 100 --brightness 0.7
run pnmixer
#run steam-native
#run firefox
#run bluemail
run lxsession
run 'nitrogen --random --set-scaled /home/zoinkshaggy/Pictures/'
