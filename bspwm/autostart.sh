#! /bin/bash

killall sxhkd 
sxhkd -c ~/.config/sxhkd/sxhkdrc ~/.config/sxhkd/apps.sh &
feh --bg-scale ~/Pictures -z &
xsetroot -cursor_name left_ptr &
killall picom
picom &
xrandr --output DisplayPort-1 --mode 3440x1440 --rate 100 --brightness 0.7 --set TearFree on &
killall lxpolkit
lxpolkit &
killall pipewire
gentoo-pipewire-launcher &
dunst &
# killall pasystray
# run pasystray
# killall polybar
# run polybar -r
~/.config/polybar/launch.sh
killall mpd
mpd &