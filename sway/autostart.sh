#! /bin/bash

killallmpd
mpd &
xsetroot -cursor_name left_ptr &
killall lxpolkit
lxpolkit &
killall dunst
dunst &
killall sway-audio-idle-inhibit
~/bin/sway-audio-idle-inhibit &
killall pipewire 
gentoo-pipewire-launcher &
