#! /bin/sh

run() {
  if ! pgrep -f "$1" ;
  then
    "$@"&
  fi
}

run picom
run feh --bg-scale ~/Pictures -z
run sxhkd -c ~/.config/sxhkd/apps.sh
run xrandr --output DisplayPort-1 --mode 3440x1440 --rate 100 --brightness 0.7
run lxsession
# run pasystray
run pipewire
run xsetroot -cursor_name left_ptr
