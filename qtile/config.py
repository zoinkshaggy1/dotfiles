# Imports   
import os
import re
import socket
import subprocess
from libqtile import qtile, hook
from libqtile.config import Click, Drag, Group, KeyChord, Key, Match, Screen, ScratchPad, DropDown
from libqtile.command import lazy
from libqtile import layout, bar, widget, hook
from libqtile.lazy import lazy
from typing import List  # noqa: F401

# Autostart script from ~/.config/qtile/autostart.sh
@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/autostart.sh'])

#Set mouse cursor
#@hook.subscribe.startup
#def runner():
#    import subprocess
#    subprocess.Popen(['xsetroot', '-cursor_name', 'left_ptr'])

import subprocess
import os
from libqtile import hook

@hook.subscribe.startup
def dbus_register():
    id = os.environ.get('DESKTOP_AUTOSTART_ID')
    if not id:
        return
    subprocess.Popen(['dbus-send',
                      '--session',
                      '--print-reply',
                      '--dest=org.gnome.SessionManager',
                      '/org/gnome/SessionManager',
                      'org.gnome.SessionManager.RegisterClient',
                      'string:qtile',
                      'string:' + id])
mod = "mod4"
mod1 = "mod1"
mod2 = "control"
mod3 = "shift"
home =  os.path.expanduser('~')
terminal = "alacritty"

keys =[
    # Change window focus
    Key([mod], "Left", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "Right", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "Down", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "Up", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "Tab", lazy.layout.next(),desc="Move window focus to other window"),

    # Move windows
    Key([mod, "shift"], "Left", lazy.layout.shuffle_left(), desc="Move window to the left"),
    Key([mod, "shift"], "Right", lazy.layout.shuffle_right(), desc="Move window to the right"),
    Key([mod, "shift"], "Down", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "Up", lazy.layout.shuffle_up(), desc="Move window up"),

    #Resize layout
    #Key([mod, "control"], "Left", lazy.layout.shrink(), lazy.layout.decrease_nmaster(), desc="Grow window to the left"),
    Key([mod, "control"], "Left", lazy.layout.grow_left(), lazy.layout.shrink(), desc='Grow Windows to the left'),
    Key([mod, "control"], "Right", lazy.layout.grow_right(), lazy.layout.grow(), desc="Grow window to the right"),
    Key([mod, "control"], "Down", lazy.layout.grow_down(), lazy.layout.grow(), desc="Grow window down"),
    Key([mod, "control"], "Up", lazy.layout.grow_up(), lazy.layout.shrink(), desc="Grow window up"),

    Key([mod], "n", lazy.layout.reset(), desc='reset layout'),

    # Toggle window states
    Key([mod, "shift"], "t", lazy.window.toggle_floating()),
    Key([mod], "f", lazy.window.toggle_fullscreen(), desc='toggle fullscreen'),
    Key([mod, "shift"], "m", lazy.layout.maximize()),
    Key([mod], "m", lazy.window.toggle_maximize(), desc='toggle window between minimum and maximum sizes'),

    # Launch user defined terminal
    Key([mod], "Return", lazy.spawn(terminal+" -e fish"), desc="Launch terminal"),

    # Toggle between different layouts as defined below
    Key([mod], "space", lazy.next_layout(), desc="Toggle between layouts"),

    # Keybinding to kill focused windows
    Key([mod], "x", lazy.window.kill(), desc="Kill focused window"),
    Key([mod, "shift"], "c", lazy.window.kill(), desc="Kill focused window"),

    # Restart or Quit Qtile
    Key([mod], "q", lazy.restart(), desc="Restart Qtile"),
    Key([mod, "shift"], "q", lazy.shutdown(), desc="Shutdown Qtile"),

    # Keybindings to launch user defined programs
    Key([mod], "p", lazy.spawn("rofi -modi drun -show drun -show-icons"), desc="launch rofi"),
    Key([mod], "w", lazy.spawn("firefox-esr"), desc="launch firefox-esr"),
    #Key([mod], "f", lazy.spawn("pcmanfm"), desc="launch pcmanfm"),

    # Audio controls
    Key([], "XF86AudioMute", lazy.spawn("pamixer -t")),
    #Key([], "XF86AudioLowerVolume", lazy.spawn("pamixer -d 5")),
    #Key([], "XF86AudioRaiseVolume", lazy.spawn("pamixer -i 5 --allow-boost")),
    #Key(["shift"], "XF86AudioRaiseVolume", lazy.spawn("pactl set-sink-volume @DEFAULT_SINK@ +5%")),
    Key(["shift"], "XF86AudioLowerVolume", lazy.spawn("pactl set-default-sink 0")),
    Key(["shift"], "XF86AudioRaiseVolume", lazy.spawn("pactl set-default-sink 1")),

    # Brightness Controls
    Key([], "XF86HomePage", lazy.spawn("xrandr --output DisplayPort-1 --brightness 0.7")),
    Key([], "XF86Explorer", lazy.spawn("xrandr --output DisplayPort-1 --brightness 0.5")),
    Key(["shift"], "XF86HomePage", lazy.spawn("xrandr --output DisplayPort-1 --brightness 1.0")),
    Key([mod], "r", lazy.spawncmd(), desc="Spawn a command using a prompt widget"),

    # ALT KEY Keybindings to launch alternative programs
    # Key([mod1], "b", lazy.spawn("wal"), desc="launch wal script"),
    Key(["control", "shift"], "e", lazy.spawn("emacsclient -c -a emacs"), desc='Doom Emacs'),
    #Key([mod1], "f", lazy.spawn("st -e vifm"), desc="launch vifm"),
    Key([mod1], "w", lazy.spawn("brave"), desc="launch brave-browser"),
    Key([mod1], "e", lazy.spawn("geany"), desc="launch geany"),
    Key([mod], "y", lazy.spawn("scrot"), desc="take screenshot"),
    Key([mod], "s", lazy.spawn("xmenu.sh && scrot"), desc="launch xmenu"),

    #Screensaver
    Key([mod], "g", lazy.spawn("xset s off -dpms"), desc="Turn off Screensaver"),
    Key([mod, "shift"], "g", lazy.spawn("xset s on +dpms"), desc="Turn on Screensaver"),

    # Passwords
    Key([mod, "shift"], "p", lazy.spawn("passmenu -fn 33"), desc="Passwords"),
    Key([mod, "control"], "p", lazy.spawn("passmenu-otp -fn 33"), desc="OTP Passwords"),

    #Reboot
    Key([mod1, "control"], "Delete", lazy.spawn("lxsession-logout")),

    ]


groups= [
                Group("1",
                      label="",
                      ),

                Group("2",
                      label="",
                      matches=[Match(wm_class=["firefox"])],
                      ),

                Group("3",
                      label="",
                      ),

                Group("4",
                      label="",
                      matches=[Match(wm_class=["qutebrowser", "BlueMail"])],
                      ),

                Group("5",
                      label=""),

                Group("6",
                      label="",
                      ),

                Group("7",
                      label="",
                      ),

                Group("8",
                      label=""),

                Group("9",
                      label="",
                      matches=[Match(wm_class=["Steam"]),
                               ],
                      ),

                Group("0",
                      label=""),

               ]

for i in range(len(groups)):
   keys.append(Key([mod], str((i)), lazy.group[str(i)].toscreen()))
   keys.append(Key([mod, "shift"], str((i)), lazy.window.togroup(str(i), switch_group=True)))

#LAYOUTS
layouts = [
     #layout.Columns(margin=7, border_width=4, border_focus="#ffffff", border_normal="#4c566a", ),
     #layout.Matrix(),
     #layout.RatioTile(margin=7)
     #layout.Tile(margin=7, border_width=3, border_focus="#ffffff", border_normal="#4c566a", new_client_position='top', ratio=0.55),
     #layout.VerticalTile(),
     #layout.Zoomy(
     #   margin=7,
     #   columnwidth=300,
     #),
    #layout.TreeTab(
    #        active_bg = 'ffffff',
    #        active_fg = '000000',
    #        bg_color = '293136',
    #        font = 'novamono for powerline',
    #        fontsize = 15,
    #        panel_width = 200,
    #        inactive_bg = 'a1acff',
    #        inactive_fg = '000000',
    #        sections = ['Qtile'],
    #        section_fontsize = 18,
    #       section_fg = 'ffffff',
    #        section_left = 70
    #),
    layout.MonadTall(margin=5, border_width=2, border_focus="red", border_normal="#4c566a"),
    #layout.MonadWide(margin=5, border_width=3, border_focus="#bc7cf7", border_normal="#4c566a"),
   # layout.Bsp      (margin=5, border_width=3, border_focus="#bc7cf7", border_normal="#4c566a", fair=False),
    layout.Max(),
    layout.Stack(num_stacks=3, margin=8),
]


colors =  [ ["#2E3440", "#2E3440"], # color 0
            ["#2E3440", "#2E3440"], # color 1
            ["#c0c5ce", "#c0c5ce"], # color 2
            ["#fba922", "#fba922"], # color 3
            ["#3384d0", "#3384d0"], # color 4
            ["#f3f4f5", "#f3f4f5"], # color 5
            ["#cd1f3f", "#cd1f3f"], # color 6
            ["#62FF00", "#62FF00"], # color 7
            ["#6790eb", "#6790eb"], # color 8
            ["#a9a9a9", "#a9a9a9"]] # color 9

widget_defaults = dict(
    font="Hack Nerd Font Mono",
    fontsize=20,
    padding=10,
    foreground=colors[5],
    background=colors[1],
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=bar.Bar(
            [
                widget.TextBox(
                    font = 'Font Awesome 6',
                    text="✏",
                    #mouse_callbacks={'Button1': lambda: qtile.cmd_spawn('emacsclient -c -a emacs /home/zoinkshaggy/.config/qtile/config.py')},
                    # mouse_callbacks={'Button1': lambda: qtile.cmd_spawn(terminal + '-e helix /home/zoinkshaggy/.config/qtile/config.py')},
                    mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(terminal + ' -e helix ~/.config/qtile/config.py')},
                ),

                widget.Sep(
                    linewidth=1,
                    foreground=colors[2],
                ),
                
                widget.GroupBox(
                    #font="Font Awesome",
                    fontsize=33,
                    margin_y=3,
                    margin_x=6,
                    padding_y=7,
                    padding_x=6,
                    borderwidth=4,
                    active=colors[4],
                    inactive=colors[5],
                    rounded=False,
                    highlight_method="text",
                    this_current_screen_border = colors[7],
                ),

                widget.Prompt(
                    background=colors[8],
                ),

                widget.Sep(
                    linewidth=1,
                    padding=10,
                    foreground=colors[2],
                ),

                widget.WindowName(
                    foreground=colors[3],
                        ),

                widget.Sep(
                    linewidth=1,
                    padding=0,
                    foreground=colors[2],
                ),

                
                widget.OpenWeather(
                    zip='84062',
                    metric=False,
                    font='FiraCode Nerd Font Mono:style=Bold',
                    fontsize=20,
                    foreground=colors[5],
                    mouse_callbacks={'Button1': lambda: qtile.cmd_spawn(terminal +" -e curl wttr.in")},
                ),

                widget.Sep(
                    linewidth=1,
                    padding=10,
                    foreground=colors[2],
                ),

                widget.CurrentLayoutIcon(
                    custom_icon_paths=[os.path.expanduser("~/.config/qtile/icons")],
                    scale=0.6,
                    padding=0,
                ),

                widget.CurrentLayout(
                ),

                 widget.Sep(
                    linewidth=1,
                    padding=10,
                    foreground=colors[2],
                ),

                widget.CheckUpdates(
                    update_interval = 60,
                    distro = "Arch_checkupdates",
                    display_format = "{updates} Updates",
                    fmt='🛠️ {}',
                    no_update_string = "0 Updates",
                    mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(terminal + ' -e sudo pacman -Syyuu')},
                    colour_have_updates=colors[7],
                ),

                widget.Sep(
                    linewidth=1,
                    padding=10,
                    foreground=colors[2],
                ),

                widget.DF(
                    fmt = ' 🏠 {} ',
                    font='UbuntuMono Nerd Font',
	            partition = '/home',
	            format = '{uf}{m} ({r:.0f}%)',
	            visible_on_warn = False,
	            padding = -1,
	            mouse_callbacks={'Button1': lambda: qtile.cmd_spawn('xdiskusage')},
				),

                widget.Sep(
                    linewidth=1,
                    padding=10,
                    foreground=colors[2],
                ),

                widget.Memory(
                    fmt=' {}',
                    font='Font Awesome',
                    format='{MemUsed: .0f} MB',
                    mouse_callbacks={'Button1': lambda: qtile.cmd_spawn(terminal + " -e bashtop")},
                    foreground=colors[7],
                ),

                widget.Sep(
                    linewidth=1,
                    padding=10,
                    foreground=colors[2],
                ),

                #widget.PulseVolume(
                #    fmt= ' {}', 
                #    get_volume_command='pamixer --get-volume-human',
                #    volume_up_command='pamixer -i 5',
                #    volume_down_command='pamixer -d 5',
                #    mute_command='pamixer -t',
                #    volume_app='pamixer',
                #    emoji=True,
                #    fontsize=25,
                #    mouse_callbacks={'Button3': lambda: qtile.cmd_spawn('pavucontrol')},
                #    padding=0,

                #),

                #widget.PulseVolume(
                #    fmt='{}',
                #),

                #widget.Sep(
                #    linewidth=1,
                #    padding=10,
                #    foreground=colors[2],
                #),

                widget.Wallpaper(
                    foreground='c5c8c6',
                    directory='~/Pictures',
                    random_selection=True,
                    label='Change Wallpaper',
                    wallpaper_command=['feh', '--bg-scale']
                ),

                widget.Sep(
                    linewidth=1,
                    padding=10,
                    foreground=colors[2],
                ),

                widget.Clock(
                    font='FiraCode Nerd Font Mono',
                    fmt=' 🗓️ {}',
                    format='%a, %b. %d %I:%M %p',
                    mouse_callbacks={'Button1': lambda: qtile.cmd_spawn(terminal + " -e calcurse")},
                    padding=0,
                ),

                widget.Sep(
                    padding=6,
                    linewidth=0,
                ),

                widget.Notify(
                ),

                widget.Sep(
                    linewidth=1,
                    padding=0,
                    foreground=colors[2],
                ),

                widget.Systray(
                    Padding=10,
                ),

                widget.Sep(
                    linewidth=0,
                    padding=10,
                ),

                widget.Sep(
                    linewidth=1,
                    padding=10,
                    foreground=colors[2],
                ),

                widget.TextBox(
                    text='  ',
                    fontsize='25',
                    foreground=colors[3],
                    font="Font Awesome",
                    padding=8,
                    mouse_callbacks={'Button1': lambda: qtile.cmd_spawn("lxsession-logout")},
                ),
            ],
        30,
            opacity=1.0,
            ),
       ),
    ]

def window_to_prev_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i - 1].name)

def window_to_next_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i + 1].name)

def window_to_previous_screen(qtile):
    i = qtile.screens.index(qtile.current_screen)
    if i != 0:
        group = qtile.screens[i - 1].group.name
        qtile.current_window.togroup(group)

def window_to_next_screen(qtile):
    i = qtile.screens.index(qtile.current_screen)
    if i + 1 != len(qtile.screens):
        group = qtile.screens[i + 1].group.name
        qtile.current_window.togroup(group)

def switch_screens(qtile):
    i = qtile.screens.index(qtile.current_screen)
    group = qtile.screens[i - 1].group
    qtile.current_screen.set_group(group)

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False

#Floating rules
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    *layout.Floating.default_float_rules,
    Match(wm_class='confirmreset'),  # gitk
    Match(wm_class='makebranch'),  # gitk
    Match(wm_class='maketag'),  # gitk
    Match(wm_class='ssh-askpass'),  # ssh-askpass
    Match(title='branchdialog'),  # gitk
    Match(title='pinentry'),  # GPG key password entry
    Match(title="Friends List"),
])
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

wmname = "LG3D"

