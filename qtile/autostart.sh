#!/usr/bin/env bash 

xrandr --output DisplayPort-1 --mode 3440x1440 --rate 120 --brightness 0.7
lxsession &
picom &
dunst &
#bluemail &
#steam-native &
pnmixer &
#nitrogen --restore &
#/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
#volumeicon &
#nm-applet &
#caffeine &
