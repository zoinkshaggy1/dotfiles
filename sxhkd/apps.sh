#! /bin/bash

# wm independent hotkeys

# terminal emulator
super + Return
	kitty

# Rofi help, calculator, Power Menu, emoji, Show open windows
super + r; {slash, space , c , p, e, Tab}
	{~/.config/rofi/scripts/help.sh, rofi -modi drun -show drun -show-icons,rofi -show calc -modi calc -no-show-match -no-sort,\
	rofi -show power-menu -modi power-menu:rofi-power-menu, rofi -show emoji -modi emoji, rofi -show}
	
# alt + p
# 	rofi-pass	
	
# Show help
super + slash
	{~/.config/rofi/scripts/help.sh}
	
#pass
super + shift + p; {d,r}
	{passmenu -fn 50, rofi-pass}

#Dmenu
super + d
	dmenu_run -fn "Hack Nerd Font 50"

# make sxhkd reload its configuration files:
super + Escape
	pkill -USR1 -x sxhkd; notify-send 'sxhkd' 'Reloaded Config'

#Brightness
{XF86Explorer,XF86HomePage,shift + XF86HomePage}
	{xrandr --output DisplayPort-1 --brightness 0.5, xrandr --output DisplayPort-1 --brightness 0.7, xrandr --output DisplayPort-1 --brightness 1}	

# Volume Controls
# {XF86AudioLowerVolume,XF86AudioRaiseVolume,XF86AudioMute}
# 	{pamixer -d 5,pamixer -i 5,pamixer -t}

#Logout
ctrl + alt + Delete
	lxsession-logout
	
#Toggle Polybar
super + b
	{polybar-msg cmd toggle; bspc config top_padding +0, polybar-msg cmd toggle; bspc config top_padding 42}
	
#System monitor tools
alt + t; {h,b}
	{kitty -e htop, kitty -e btop}

#LF File Manager
super + shift + f
	alacritty -e lfrun

#Close window
super + q
	xdo close
	
#Screensaver
super + {_,shift +}g
	xset s {off -dpms, on +dpms}