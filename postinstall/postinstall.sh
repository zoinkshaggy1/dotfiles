#!/bin/bash
sudo cp -v /home/dotfiles/etc/makepkg.conf /etc/
sudo rm /etc/doas.conf
sudo su -c 'echo "permit nopass :wheel" >> /etc/doas.conf'
sudo pacman -Syy
git clone https://aur.archlinux.org/paru.git
cd paru
makepkg -si
paru -S - < /home/dotfiles/postinstall/pkgs
cd /home
doas git clone https://gitlab.com/zoinkshaggy1/dotfiles.git
doas git clone https://gitlab.com/zoinkshaggy/pass.git
doas chown -R $USER dotfiles
doas chown -R $USER pass
ln -s /home/pass/ ~/.password-store
gpg --import ~/.password-store/private.gpg
ln -s /home/dotfiles/paru/ ~/.config/
ln -s /home/dotfiles/alacritty/ ~/.config/
ln -s /home/dotfiles/kitty/ ~/.config/
ln -s /home/dotfiles/qtile/ ~/.config/
ln -s /home/dotfiles/spectrwm/ ~/.config/
chsh -s /usr/bin/fish zoinkshaggy
systemctl enable ly
systemctl enable reflector.timer